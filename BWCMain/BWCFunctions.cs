﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Management.Automation;
using System.Management.Automation.Runspaces;
using System.Data.Linq.Mapping;
using System.Data.Linq;
using System.Net;
using System.Data.SqlClient;
using System.IO;
using System.Data;

namespace BWCMain
{
    public class BWCFunctions
    {

        public void FTPFiles()
        {
            // Set the values of all variables of the function
            string FTPUserName = Properties.Settings.Default.FTPUserName;
            string FTPPassword = Properties.Settings.Default.FTPPassword;
            string CurrentFilesTable = Properties.Settings.Default.CurrentFilesTable;
            string SettingsTable = Properties.Settings.Default.SettingsTable;
            string Destination = "";
            string Source = "";

            DataSet dsCurrentFiles = new DataSet();

            //Set up a connection to sql
            SqlConnection myConnection = new SqlConnection(Properties.Settings.Default.DBConnectionString);
            myConnection.Open();

            //Get a list of files that we are looking for
            SqlCommand myCommand = new SqlCommand("select * from " + CurrentFilesTable, myConnection);
            SqlDataAdapter myDataAdapter = new SqlDataAdapter(myCommand);
            myDataAdapter.Fill(dsCurrentFiles);

            //get where we are pulling the files from
            myCommand = new SqlCommand("select value from " + SettingsTable + " where Id = 3", myConnection);
            Source = (string)myCommand.ExecuteScalar();

            //get the location where we should save the files too
            myCommand = new SqlCommand("select value from " + SettingsTable + " where Id = 1", myConnection);
            Destination = (string)myCommand.ExecuteScalar();

            foreach (DataRow dr in dsCurrentFiles.Tables[0].Rows)
            {
                string FileDestination = "";
                string FileSource = "";

                //Get the record Id, file name and most recent file date we are processing
                Int32 RecordId = Convert.ToInt32(dr["Id"]);
                String FileName = dr["FileName"].ToString();
                DateTime MostRecentFileDate = Convert.ToDateTime(dr["MostRecentFileDate"]);

                //Update the source and destination paths with the current file we are processing
                FileSource = Source + "/" + FileName;
                FileDestination = Destination + "\\" + FileName;

                //Connect to the ftp site and get the Current file date on the ftp site
                FtpWebRequest requestTimeStamp = FtpWebRequest.Create(FileSource) as FtpWebRequest;
                requestTimeStamp.Credentials = new NetworkCredential(FTPUserName, FTPPassword);
                requestTimeStamp.Method = WebRequestMethods.Ftp.GetDateTimestamp;
                FtpWebResponse responseTimeStamp = requestTimeStamp.GetResponse() as FtpWebResponse;
                DateTime CurrentFileDate = Convert.ToDateTime(responseTimeStamp.LastModified);
                responseTimeStamp.Close();

                //check to see if the date of the file on the ftp site is greater than what is in the table that we last processed
                //if the file in newer then download it and update the values in the table, if not then skip it
                if (CurrentFileDate > MostRecentFileDate)
                {
                    //Tell the ftp site we want to download the file
                    FtpWebRequest requestDownloadFile = FtpWebRequest.Create(FileSource) as FtpWebRequest;
                    requestDownloadFile.Credentials = new NetworkCredential(FTPUserName, FTPPassword);
                    requestDownloadFile.Method = WebRequestMethods.Ftp.DownloadFile;
                    FtpWebResponse responseDownloadFile = requestDownloadFile.GetResponse() as FtpWebResponse;

                    //Read the file and write to the destination
                    Stream responseStream = responseDownloadFile.GetResponseStream();

                    //Create the target file on the local system and the download buffer
                    byte[] buffer = new byte[2048];
                    FileStream fs = new FileStream(FileDestination, FileMode.Create);
                    int ReadCount = responseStream.Read(buffer, 0, buffer.Length);
                    while (ReadCount > 0)
                    {
                        fs.Write(buffer, 0, ReadCount);
                        ReadCount = responseStream.Read(buffer, 0, buffer.Length);
                    }

                    string ResponseDescription = responseDownloadFile.StatusDescription;
                    fs.Close();
                    responseStream.Close();

                    responseDownloadFile.Close();

                    //Update the IsReady flag and most recent file date in the table so we know this file is ready to be processed
                    myCommand = new SqlCommand("update " + CurrentFilesTable + " set IsReady = 1, MostRecentFileDate = '" + CurrentFileDate + "', ModDate = '" + DateTime.Now + "' where Id = " + RecordId, myConnection);
                    myCommand.ExecuteNonQuery();
                }
            }
        }
    }
}
