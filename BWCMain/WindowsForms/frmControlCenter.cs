﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BWCMain
{
    public partial class frmBWCControlCenter : Form
    {
        public frmBWCControlCenter()
        {
            InitializeComponent();

        }

        private void btnDownloadFiles_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;

            BWCFunctions BWCFunctionsClass = new BWCFunctions();

            BWCFunctionsClass.FTPFiles();

            Cursor.Current = Cursors.Default;
        }

        private void manageRequestFileToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }
    }
}
