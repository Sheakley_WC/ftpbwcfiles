﻿namespace BWCMain
{
    partial class frmBWCControlCenter
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.frmBWCControlCenter_Fill_Panel = new Infragistics.Win.Misc.UltraPanel();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.bWCRequestsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.manageRequestFileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.historyToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.jobMaintenanceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.bWCDownloadToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.manageBWCDownloadToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.errorManagementToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.processHistoryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.historyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.jobMaintenanceToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.filesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.viewFileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.settingsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.lookupTableMaintenanceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.usersToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.btnDownloadFiles = new System.Windows.Forms.Button();
            this.frmBWCControlCenter_Fill_Panel.ClientArea.SuspendLayout();
            this.frmBWCControlCenter_Fill_Panel.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // frmBWCControlCenter_Fill_Panel
            // 
            // 
            // frmBWCControlCenter_Fill_Panel.ClientArea
            // 
            this.frmBWCControlCenter_Fill_Panel.ClientArea.Controls.Add(this.btnDownloadFiles);
            this.frmBWCControlCenter_Fill_Panel.ClientArea.Controls.Add(this.menuStrip1);
            this.frmBWCControlCenter_Fill_Panel.Cursor = System.Windows.Forms.Cursors.Default;
            this.frmBWCControlCenter_Fill_Panel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.frmBWCControlCenter_Fill_Panel.Location = new System.Drawing.Point(0, 0);
            this.frmBWCControlCenter_Fill_Panel.Name = "frmBWCControlCenter_Fill_Panel";
            this.frmBWCControlCenter_Fill_Panel.Size = new System.Drawing.Size(693, 346);
            this.frmBWCControlCenter_Fill_Panel.TabIndex = 0;
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bWCRequestsToolStripMenuItem,
            this.bWCDownloadToolStripMenuItem,
            this.filesToolStripMenuItem,
            this.settingsToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(693, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // bWCRequestsToolStripMenuItem
            // 
            this.bWCRequestsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.manageRequestFileToolStripMenuItem,
            this.historyToolStripMenuItem1,
            this.jobMaintenanceToolStripMenuItem});
            this.bWCRequestsToolStripMenuItem.Name = "bWCRequestsToolStripMenuItem";
            this.bWCRequestsToolStripMenuItem.Size = new System.Drawing.Size(95, 20);
            this.bWCRequestsToolStripMenuItem.Text = "BWC Requests";
            // 
            // manageRequestFileToolStripMenuItem
            // 
            this.manageRequestFileToolStripMenuItem.Name = "manageRequestFileToolStripMenuItem";
            this.manageRequestFileToolStripMenuItem.Size = new System.Drawing.Size(190, 22);
            this.manageRequestFileToolStripMenuItem.Text = "Request Management";
            this.manageRequestFileToolStripMenuItem.Click += new System.EventHandler(this.manageRequestFileToolStripMenuItem_Click);
            // 
            // historyToolStripMenuItem1
            // 
            this.historyToolStripMenuItem1.Name = "historyToolStripMenuItem1";
            this.historyToolStripMenuItem1.Size = new System.Drawing.Size(190, 22);
            this.historyToolStripMenuItem1.Text = "Job History";
            // 
            // jobMaintenanceToolStripMenuItem
            // 
            this.jobMaintenanceToolStripMenuItem.Name = "jobMaintenanceToolStripMenuItem";
            this.jobMaintenanceToolStripMenuItem.Size = new System.Drawing.Size(190, 22);
            this.jobMaintenanceToolStripMenuItem.Text = "Job Maintenance";
            // 
            // bWCDownloadToolStripMenuItem
            // 
            this.bWCDownloadToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.manageBWCDownloadToolStripMenuItem,
            this.errorManagementToolStripMenuItem,
            this.processHistoryToolStripMenuItem,
            this.historyToolStripMenuItem,
            this.jobMaintenanceToolStripMenuItem1});
            this.bWCDownloadToolStripMenuItem.Name = "bWCDownloadToolStripMenuItem";
            this.bWCDownloadToolStripMenuItem.Size = new System.Drawing.Size(145, 20);
            this.bWCDownloadToolStripMenuItem.Text = "BWC Download Process";
            // 
            // manageBWCDownloadToolStripMenuItem
            // 
            this.manageBWCDownloadToolStripMenuItem.Name = "manageBWCDownloadToolStripMenuItem";
            this.manageBWCDownloadToolStripMenuItem.Size = new System.Drawing.Size(178, 22);
            this.manageBWCDownloadToolStripMenuItem.Text = "Create New Process";
            // 
            // errorManagementToolStripMenuItem
            // 
            this.errorManagementToolStripMenuItem.Name = "errorManagementToolStripMenuItem";
            this.errorManagementToolStripMenuItem.Size = new System.Drawing.Size(178, 22);
            this.errorManagementToolStripMenuItem.Text = "Error Management";
            // 
            // processHistoryToolStripMenuItem
            // 
            this.processHistoryToolStripMenuItem.Name = "processHistoryToolStripMenuItem";
            this.processHistoryToolStripMenuItem.Size = new System.Drawing.Size(178, 22);
            this.processHistoryToolStripMenuItem.Text = "Process History";
            // 
            // historyToolStripMenuItem
            // 
            this.historyToolStripMenuItem.Name = "historyToolStripMenuItem";
            this.historyToolStripMenuItem.Size = new System.Drawing.Size(178, 22);
            this.historyToolStripMenuItem.Text = "Job History";
            // 
            // jobMaintenanceToolStripMenuItem1
            // 
            this.jobMaintenanceToolStripMenuItem1.Name = "jobMaintenanceToolStripMenuItem1";
            this.jobMaintenanceToolStripMenuItem1.Size = new System.Drawing.Size(178, 22);
            this.jobMaintenanceToolStripMenuItem1.Text = "Job Maintenance";
            // 
            // filesToolStripMenuItem
            // 
            this.filesToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.viewFileToolStripMenuItem});
            this.filesToolStripMenuItem.Name = "filesToolStripMenuItem";
            this.filesToolStripMenuItem.Size = new System.Drawing.Size(42, 20);
            this.filesToolStripMenuItem.Text = "Files";
            // 
            // viewFileToolStripMenuItem
            // 
            this.viewFileToolStripMenuItem.Name = "viewFileToolStripMenuItem";
            this.viewFileToolStripMenuItem.Size = new System.Drawing.Size(125, 22);
            this.viewFileToolStripMenuItem.Text = "View Files";
            // 
            // settingsToolStripMenuItem
            // 
            this.settingsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lookupTableMaintenanceToolStripMenuItem,
            this.usersToolStripMenuItem});
            this.settingsToolStripMenuItem.Name = "settingsToolStripMenuItem";
            this.settingsToolStripMenuItem.Size = new System.Drawing.Size(61, 20);
            this.settingsToolStripMenuItem.Text = "Settings";
            // 
            // lookupTableMaintenanceToolStripMenuItem
            // 
            this.lookupTableMaintenanceToolStripMenuItem.Name = "lookupTableMaintenanceToolStripMenuItem";
            this.lookupTableMaintenanceToolStripMenuItem.Size = new System.Drawing.Size(217, 22);
            this.lookupTableMaintenanceToolStripMenuItem.Text = "Lookup Table Maintenance";
            // 
            // usersToolStripMenuItem
            // 
            this.usersToolStripMenuItem.Name = "usersToolStripMenuItem";
            this.usersToolStripMenuItem.Size = new System.Drawing.Size(217, 22);
            this.usersToolStripMenuItem.Text = "Users";
            // 
            // btnDownloadFiles
            // 
            this.btnDownloadFiles.Location = new System.Drawing.Point(485, 128);
            this.btnDownloadFiles.Name = "btnDownloadFiles";
            this.btnDownloadFiles.Size = new System.Drawing.Size(75, 23);
            this.btnDownloadFiles.TabIndex = 1;
            this.btnDownloadFiles.Text = "button1";
            this.btnDownloadFiles.UseVisualStyleBackColor = true;
            this.btnDownloadFiles.Click += new System.EventHandler(this.btnDownloadFiles_Click);
            // 
            // frmBWCControlCenter
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(693, 346);
            this.Controls.Add(this.frmBWCControlCenter_Fill_Panel);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "frmBWCControlCenter";
            this.Text = "BWC Control Center";
            this.frmBWCControlCenter_Fill_Panel.ClientArea.ResumeLayout(false);
            this.frmBWCControlCenter_Fill_Panel.ClientArea.PerformLayout();
            this.frmBWCControlCenter_Fill_Panel.ResumeLayout(false);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private Infragistics.Win.Misc.UltraPanel frmBWCControlCenter_Fill_Panel;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem bWCRequestsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem manageRequestFileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem jobMaintenanceToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem bWCDownloadToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem manageBWCDownloadToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem jobMaintenanceToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem historyToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem filesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem viewFileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem historyToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem processHistoryToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem settingsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem lookupTableMaintenanceToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem usersToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem errorManagementToolStripMenuItem;
        private System.Windows.Forms.Button btnDownloadFiles;
    }
}

