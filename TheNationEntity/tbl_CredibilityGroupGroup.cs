//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace TheNationEntity
{
    using System;
    using System.Collections.Generic;
    
    public partial class tbl_CredibilityGroupGroup
    {
        public long ID { get; set; }
        public byte GroupNum { get; set; }
        public int rateYR { get; set; }
        public Nullable<int> EmployerTypeID { get; set; }
        public Nullable<long> MinExpectedLoss { get; set; }
        public Nullable<long> MaxExpectedLoss { get; set; }
        public Nullable<long> MaxValue { get; set; }
        public Nullable<decimal> GroupCredibilityPct { get; set; }
        public Nullable<decimal> CX_Val { get; set; }
        public Nullable<decimal> Z_Val { get; set; }
        public Nullable<System.DateTime> ModDate { get; set; }
        public string ModBy { get; set; }
    
        public virtual tbl_EmployerTypes tbl_EmployerTypes { get; set; }
    }
}
