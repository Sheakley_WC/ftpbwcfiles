//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace TheNationEntity
{
    using System;
    using System.Collections.Generic;
    
    public partial class RequestedPolicy
    {
        public long PolicyNum { get; set; }
        public Nullable<bool> IsProcessed { get; set; }
        public Nullable<bool> IsErrored { get; set; }
        public string ErrorMsg { get; set; }
    }
}
