//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace TheNationEntity
{
    using System;
    using System.Collections.Generic;
    
    public partial class tbl_MREMP20_BWC
    {
        public long PolicyNum { get; set; }
        public Nullable<int> BusSeqNum { get; set; }
        public int rateYR { get; set; }
        public Nullable<byte> PolicyTypeID { get; set; }
        public string ManCode { get; set; }
        public string SubManCode { get; set; }
        public Nullable<long> Payroll { get; set; }
        public Nullable<decimal> BaseRate { get; set; }
        public Nullable<decimal> ExpectedLossRate { get; set; }
        public Nullable<long> ExpectedLoss { get; set; }
        public string isMeritRated { get; set; }
        public Nullable<byte> PolicyManStatusID { get; set; }
        public Nullable<decimal> LimitedLossRate { get; set; }
        public Nullable<long> LimitedLoss { get; set; }
        public Nullable<System.DateTime> ModDate { get; set; }
        public string ModBy { get; set; }
    
        public virtual tbl_MREMP10_BWC tbl_MREMP10_BWC { get; set; }
        public virtual tbl_WCPolicyMaster tbl_WCPolicyMaster { get; set; }
    }
}
