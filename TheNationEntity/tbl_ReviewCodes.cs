//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace TheNationEntity
{
    using System;
    using System.Collections.Generic;
    
    public partial class tbl_ReviewCodes
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public tbl_ReviewCodes()
        {
            this.tbl_PolicyAttributes = new HashSet<tbl_PolicyAttributes>();
        }
    
        public int ID { get; set; }
        public string Description { get; set; }
        public string DefaultStatus { get; set; }
        public Nullable<long> CautionFlagID { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbl_PolicyAttributes> tbl_PolicyAttributes { get; set; }
    }
}
