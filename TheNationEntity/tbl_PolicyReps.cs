//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace TheNationEntity
{
    using System;
    using System.Collections.Generic;
    
    public partial class tbl_PolicyReps
    {
        public long PolicyNum { get; set; }
        public int RepTypeID { get; set; }
        public long RepId { get; set; }
        public int RepLevel { get; set; }
        public string ModBy { get; set; }
        public Nullable<System.DateTime> ModDate { get; set; }
    
        public virtual tbl_WCPolicyMaster tbl_WCPolicyMaster { get; set; }
    }
}
