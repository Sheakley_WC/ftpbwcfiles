//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace TheNationEntity
{
    using System;
    using System.Collections.Generic;
    
    public partial class tbl_RepPolicyAssignmentsBackup
    {
        public long ID { get; set; }
        public long PolicyNum { get; set; }
        public long RepID { get; set; }
        public Nullable<long> InsideRepID { get; set; }
        public System.DateTime ModDate { get; set; }
        public string ModBy { get; set; }
        public byte[] RowVersion { get; set; }
    }
}
