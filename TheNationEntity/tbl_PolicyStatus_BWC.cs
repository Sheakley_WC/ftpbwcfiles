//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace TheNationEntity
{
    using System;
    using System.Collections.Generic;
    
    public partial class tbl_PolicyStatus_BWC
    {
        public long ID { get; set; }
        public Nullable<long> PolicyNum { get; set; }
        public Nullable<int> BusSeqNum { get; set; }
        public Nullable<System.DateTime> StartDate { get; set; }
        public Nullable<System.DateTime> EndDate { get; set; }
        public string PolicyStatusCode { get; set; }
        public Nullable<System.DateTime> ModDate { get; set; }
        public string ModBy { get; set; }
        public Nullable<System.DateTime> AcceptDate { get; set; }
    }
}
