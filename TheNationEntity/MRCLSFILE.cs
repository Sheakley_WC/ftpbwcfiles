//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace TheNationEntity
{
    using System;
    using System.Collections.Generic;
    
    public partial class MRCLSFILE
    {
        public int ID { get; set; }
        public string RepresentativeNumber { get; set; }
        public string RepresentativeType { get; set; }
        public string RecordType { get; set; }
        public string RequestorNumber { get; set; }
        public string PolicyType { get; set; }
        public string PolicySequenceNumber { get; set; }
        public string BusinessSequenceNumber { get; set; }
        public string isValidPolicyNumber { get; set; }
        public string isValidManualReclassification { get; set; }
        public string ReclassedFromManualNumber { get; set; }
        public string ReclassedToManualNumber { get; set; }
        public string ReclassManualCoverageType { get; set; }
        public string ReclassCreationDate { get; set; }
        public string hasReclassedPayrollInformation { get; set; }
        public string PayrollReportingPeriodFromDate { get; set; }
        public string PayrollReportingPeriodToDate { get; set; }
        public string ReclassedToManualPayrollTotal { get; set; }
        public string ErrorMessage { get; set; }
        public Nullable<long> PolicyNum { get; set; }
    }
}
