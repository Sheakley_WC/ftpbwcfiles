﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data.EntityClient;


namespace TheNationEntity
{
    public class Connection
    {
        public string databaseName { get; set; }
        public string serverName { get; set; }

        public Connection(string server, string dataBase)
        {
            serverName = server;
            databaseName = dataBase;
        }

        private string EdmxFileName
        {
            get
            {
                string value;

                switch (databaseName)
                {
                    case "WC":
                        {
                            value = "WCDataModel";
                            break;
                        }
                    case "Corp":
                        {
                            value = "CorpDataModel";
                            break;
                        }
                    default:
                        {
                            value = "";
                            break;
                        }
                }

                return value;
            }
         
        }

        public string SqlConnectionString
        {
            get
            {
                SqlConnectionStringBuilder providerCs = new SqlConnectionStringBuilder();

                providerCs.DataSource = serverName;
                providerCs.InitialCatalog = databaseName;
                providerCs.IntegratedSecurity = true;

                var csBuilder = new EntityConnectionStringBuilder();

                csBuilder.Provider = "System.Data.SqlClient";
                csBuilder.ProviderConnectionString = providerCs.ToString();

                csBuilder.Metadata = string.Format("res://{0}/{1}.csdl|res://{0}/{1}.ssdl|res://{0}/{1}.msl",
                    typeof(WCEntities).Assembly.FullName, EdmxFileName);

                return csBuilder.ToString();

            }
        }

    }
}
