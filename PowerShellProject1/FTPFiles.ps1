﻿Set-ExecutionPolicy Bypass -Scope Process
Set-ExecutionPolicy Bypass -Scope CurrentUser

#param(
	 
#	[parameter(Mandatory=$TRUE,Position=0)] 
#	[String] $Server 

#)

try{
	#Make sure the directory is empty
	Remove-Item C:\test\CurrentFiles\*

	#Set the values of all parameters of the function
	$Username = "suni"
	$Password = "Hkg9hKPZ"

	#First, read the $Source and $Target values from the list 
	$List = Import-Csv C:\Test\FilesToDownload.csv
 
	#See if we have all the files and only start downloading the files if we have them all
	ForEach ($entry in $list){ 
 
		#Set values 
		$RemoteFile = $($entry.SourceFile)
		$LastModified = $($entry.LastModified)

		#Set Defaults in case file has a blank value
		If ($LastModified -eq "") {
			$LastModified = "1-1-1900"
		}

		#Connect to ftp site and get date of file
		$FTPRequest = [System.Net.FtpWebRequest]::Create($RemoteFile) 
		$FTPRequest.Credentials = New-Object System.Net.NetworkCredential($Username,$Password) 
		$FTPRequest.Method = [System.Net.WebRequestMethods+Ftp]:: GetDateTimeStamp
		$FTPResponse = $FTPRequest.GetResponse() 

		$StatusDescription = $FTPResponse.StatusDescription
		$StatusDescription = $StatusDescription.Substring($StatusDescription.LastIndexOf(" ")+1) 
		$FileCreationDateTime=$StatusDescription.Substring(0, 4) + "-" + $StatusDescription.Substring(4, 2) + "-" + $StatusDescription.Substring(6, 2) + " " + $StatusDescription.Substring(8, 2) + ":" + $StatusDescription.Substring(10, 2) + ":" + $StatusDescription.Substring(12, 2)


		if ([DateTime]$FileCreationDateTime -gt [DateTime]$LastModified) {
			#Update the file with the new file date
			$entry.LastModified = $FileCreationDateTime
		}
		else{
			write "No New Files"
			throw "No New Files"
		}

	}

	#Use a ForEach loop to process all files
	ForEach ($entry in $list){ 
 
		#Set values 
		$RemoteFile = $($entry.SourceFile)
		$LocalFile = $($entry.TargetFile)

		#Connect to ftp site and download file
		$FTPRequest2 = [System.Net.FtpWebRequest]::Create($RemoteFile) 
		$FTPRequest2.Credentials = New-Object System.Net.NetworkCredential($Username,$Password) 
		$FTPRequest2.Method = [System.Net.WebRequestMethods+Ftp]::DownloadFile
		$FTPResponse = $FTPRequest2.GetResponse() 

		# Get a download stream from the server response 
		$ResponseStream = $FTPResponse.GetResponseStream() 

		# Create the target file on the local system and the download buffer 
		$LocalFile = New-Object IO.FileStream ($LocalFile,[IO.FileMode]::Create) 
		[byte[]]$ReadBuffer = New-Object byte[] 1024 

		# Loop through and write the file
			do { 
				$ReadLength = $ResponseStream.Read($ReadBuffer,0,1024) 
				$LocalFile.Write($ReadBuffer,0,$ReadLength) 
			} 
			while ($ReadLength -ne 0)

		$LocalFile.Close()
}

#Write the new dates to the file
$List | Export-Csv C:\Test\FilesToDownload.csv -NoType
}

Catch{
	write $error[0]
	throw $error[0]
}



