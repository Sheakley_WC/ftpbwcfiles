﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Management.Automation;

namespace BWCFTPFiles
{

    public class Get_BWCFiles
    {
        string script = "Get-FTPFiles () {}";

        private void FTPFiles()
        {
            using (var powershell = PowerShell.Create())
            {
                powershell.AddScript(script, false);

                powershell.Invoke();

                powershell.Commands.Clear();

                powershell.AddCommand("Get-FTPFiles");

                var results = powershell.Invoke();

            }
        }

    }
}
