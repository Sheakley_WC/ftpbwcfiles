﻿namespace BWCFTPFiles
{
    partial class BWCControlCenter
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.chkDEMOC = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.chkSC220 = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.chkMRCLS = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.chkMREMP = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.chkPCOMB = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.chkPHMGN = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.grpFileSelection = new Infragistics.Win.Misc.UltraGroupBox();
            this.chkAutoStart = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.BWCControlCenter_Fill_Panel = new Infragistics.Win.Misc.UltraPanel();
            this.btnRunNow = new Infragistics.Win.Misc.UltraButton();
            ((System.ComponentModel.ISupportInitialize)(this.chkDEMOC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSC220)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkMRCLS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkMREMP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkPCOMB)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkPHMGN)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grpFileSelection)).BeginInit();
            this.grpFileSelection.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkAutoStart)).BeginInit();
            this.BWCControlCenter_Fill_Panel.ClientArea.SuspendLayout();
            this.BWCControlCenter_Fill_Panel.SuspendLayout();
            this.SuspendLayout();
            // 
            // chkDEMOC
            // 
            this.chkDEMOC.Location = new System.Drawing.Point(22, 12);
            this.chkDEMOC.Name = "chkDEMOC";
            this.chkDEMOC.Size = new System.Drawing.Size(75, 20);
            this.chkDEMOC.TabIndex = 0;
            this.chkDEMOC.Text = "DEMOC";
            // 
            // chkSC220
            // 
            this.chkSC220.Location = new System.Drawing.Point(22, 38);
            this.chkSC220.Name = "chkSC220";
            this.chkSC220.Size = new System.Drawing.Size(75, 20);
            this.chkSC220.TabIndex = 1;
            this.chkSC220.Text = "SC220";
            // 
            // chkMRCLS
            // 
            this.chkMRCLS.Location = new System.Drawing.Point(22, 64);
            this.chkMRCLS.Name = "chkMRCLS";
            this.chkMRCLS.Size = new System.Drawing.Size(75, 20);
            this.chkMRCLS.TabIndex = 2;
            this.chkMRCLS.Text = "MRCLS";
            // 
            // chkMREMP
            // 
            this.chkMREMP.Location = new System.Drawing.Point(22, 90);
            this.chkMREMP.Name = "chkMREMP";
            this.chkMREMP.Size = new System.Drawing.Size(75, 20);
            this.chkMREMP.TabIndex = 3;
            this.chkMREMP.Text = "MREMP";
            // 
            // chkPCOMB
            // 
            this.chkPCOMB.Location = new System.Drawing.Point(22, 116);
            this.chkPCOMB.Name = "chkPCOMB";
            this.chkPCOMB.Size = new System.Drawing.Size(75, 20);
            this.chkPCOMB.TabIndex = 4;
            this.chkPCOMB.Text = "PCOMB";
            // 
            // chkPHMGN
            // 
            this.chkPHMGN.Location = new System.Drawing.Point(22, 142);
            this.chkPHMGN.Name = "chkPHMGN";
            this.chkPHMGN.Size = new System.Drawing.Size(75, 20);
            this.chkPHMGN.TabIndex = 5;
            this.chkPHMGN.Text = "PHMGN";
            // 
            // grpFileSelection
            // 
            this.grpFileSelection.Controls.Add(this.chkMREMP);
            this.grpFileSelection.Controls.Add(this.chkDEMOC);
            this.grpFileSelection.Controls.Add(this.chkPHMGN);
            this.grpFileSelection.Controls.Add(this.chkSC220);
            this.grpFileSelection.Controls.Add(this.chkPCOMB);
            this.grpFileSelection.Controls.Add(this.chkMRCLS);
            this.grpFileSelection.Location = new System.Drawing.Point(34, 76);
            this.grpFileSelection.Name = "grpFileSelection";
            this.grpFileSelection.Size = new System.Drawing.Size(118, 176);
            this.grpFileSelection.TabIndex = 6;
            // 
            // chkAutoStart
            // 
            this.chkAutoStart.Location = new System.Drawing.Point(16, 51);
            this.chkAutoStart.Name = "chkAutoStart";
            this.chkAutoStart.Size = new System.Drawing.Size(236, 20);
            this.chkAutoStart.TabIndex = 7;
            this.chkAutoStart.Text = "Auto Start When New Files Available";
            // 
            // BWCControlCenter_Fill_Panel
            // 
            // 
            // BWCControlCenter_Fill_Panel.ClientArea
            // 
            this.BWCControlCenter_Fill_Panel.ClientArea.Controls.Add(this.btnRunNow);
            this.BWCControlCenter_Fill_Panel.ClientArea.Controls.Add(this.chkAutoStart);
            this.BWCControlCenter_Fill_Panel.ClientArea.Controls.Add(this.grpFileSelection);
            this.BWCControlCenter_Fill_Panel.Cursor = System.Windows.Forms.Cursors.Default;
            this.BWCControlCenter_Fill_Panel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.BWCControlCenter_Fill_Panel.Location = new System.Drawing.Point(0, 0);
            this.BWCControlCenter_Fill_Panel.Name = "BWCControlCenter_Fill_Panel";
            this.BWCControlCenter_Fill_Panel.Size = new System.Drawing.Size(734, 269);
            this.BWCControlCenter_Fill_Panel.TabIndex = 0;
            // 
            // btnRunNow
            // 
            this.btnRunNow.Location = new System.Drawing.Point(16, 12);
            this.btnRunNow.Name = "btnRunNow";
            this.btnRunNow.Size = new System.Drawing.Size(75, 23);
            this.btnRunNow.TabIndex = 8;
            this.btnRunNow.Text = "Run Now";
            this.btnRunNow.Click += new System.EventHandler(this.btnRunNow_Click);
            // 
            // BWCControlCenter
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(734, 269);
            this.Controls.Add(this.BWCControlCenter_Fill_Panel);
            this.Name = "BWCControlCenter";
            this.Text = "BWC Control Center";
            ((System.ComponentModel.ISupportInitialize)(this.chkDEMOC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSC220)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkMRCLS)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkMREMP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkPCOMB)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkPHMGN)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grpFileSelection)).EndInit();
            this.grpFileSelection.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chkAutoStart)).EndInit();
            this.BWCControlCenter_Fill_Panel.ClientArea.ResumeLayout(false);
            this.BWCControlCenter_Fill_Panel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.UltraWinEditors.UltraCheckEditor chkDEMOC;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor chkSC220;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor chkMRCLS;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor chkMREMP;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor chkPCOMB;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor chkPHMGN;
        private Infragistics.Win.Misc.UltraGroupBox grpFileSelection;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor chkAutoStart;
        private Infragistics.Win.Misc.UltraPanel BWCControlCenter_Fill_Panel;
        private Infragistics.Win.Misc.UltraButton btnRunNow;
    }
}